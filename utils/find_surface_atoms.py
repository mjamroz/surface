#!/usr/bin/env python3
# Mon Apr 27 13:35:26 CEST 2015
import re
import os
import sys
import subprocess as sp
import numpy as np
import tempfile

grid = 1 # grid space of output surface
cutoff_surface_atom = 2.0 # angstrom. distance between atom and surface point
probe = 1.5  # water probe

class SurfaceResidues:
    def __init__(self, pdb_file):
        self.cwd = os.path.dirname(os.path.realpath(__file__))+"/"
        with open(pdb_file, "r") as fr:
            t = re.findall("^(ATOM.*)$", fr.read(), flags=re.M)
            print("Number of atoms: %d" % len(t), file=sys.stderr)
            self.atoms = "\n".join(t)
            # grep protein structure
            rr = re.findall("^ATOM.{9}(...).(...).(.)(....)....(.{8})(.{8})(.{8}).*$", self.atoms, flags=re.M)
            self.atoms_nr = len(rr)

            structure = np.asarray([i for j in rr for i in j]).reshape(self.atoms_nr, 7)
            self.atom_names = structure[:][:,0]
            self.res_names = structure[:][:,1]
            self.chains = structure[:][:,2]
            self.res_idx = structure[:][:,3]
            self.structure = np.array(structure[:][:,[4,5,6]], dtype=np.float32)

            fd, self.tmp_name = tempfile.mkstemp(dir='.', text=True)
            # write input for voss
            with os.fdopen(fd, "w") as fw:
                fw.write(self.atoms)
            self._runvoss()
            self.indices = self._surfaceAtomsIndices()
            
    def _runvoss(self):
        ''' run MG soft for surface calculation'''
        global grid, probe
        fd2, self.tmp_name_2 = tempfile.mkstemp(dir='.', text=True)
        fd3, self.tmp_name_3 = tempfile.mkstemp(dir='.', text=True)
        # convert to xyz with radii
        sp.call(self.cwd+"xyzr/pdb_to_xyzr "+self.tmp_name+" > "+self.tmp_name_2, shell=True)
        # calc volume
        cmd = self.cwd+"Volume.exe -p "+str(probe)+"  -i "+self.tmp_name_2+" -g "+str(grid)+" -o "+self.tmp_name_3+" &>/dev/null"
        sp.call(cmd, shell=True)
        # get surface coordinates
        with os.fdopen(fd3, "r") as fr:
            r =  re.findall("^HETATM.{24}(.{8})(.{8})(.{8}).*$", fr.read(), flags=re.M)
            s = [i for j in r for i in j]
            self.surface = np.asarray(s, dtype=np.float32).reshape(len(r), 3)
            self.surf_len = len(r)

    def surfaceResiduesPDB(self):
        a = self.atoms.split("\n")
        residx = set([self.res_idx[i] for i in self.indices])
        out = [a[i] for i in range(self.res_idx.size) if self.res_idx[i] in residx]
        print("Number of residues %d (atoms: %d)" % (len(residx), len(out)), file=sys.stderr)
        return out

    def surfaceAtomsPDB(self):
        '''Get atoms in PDB file format, closer than `cutoff` to the surface'''
        a = self.atoms.split("\n")
        subl = [a[i] for i in range(len(a)) if i in self.indices]
        return subl

    def _surfaceAtomsIndices(self):
        global cutoff_surface_atom
        cc2 = cutoff_surface_atom*cutoff_surface_atom
        cont = []
        for j in range(self.atoms_nr):
            t = np.where(np.sum((self.surface-self.structure[j])**2,axis=1) <= cc2)
            if t[0].size != 0:
                cont.append(j)
        print("Number of surface atoms: %d" % len(cont), file=sys.stderr)
        return cont

    def surfaceAtomsIndices(self):
        '''Get indexes of atoms (not PDB indexes, but array indexes)'''
        return self.indices

    def surfaceResidues(self):
        '''Get residue indexes and names < `cutoff_surface_atom` to surface points'''

        out = []
        for i in self.indices:
            s = (self.res_idx[i]+self.res_names[i]).strip()
            if s not in out:
                out.append(s)
        return out

    def __del__(self):
        # delete temporary files
        os.unlink(self.tmp_name)
        if hasattr(self, 'tmp_name_2'):
            os.unlink(self.tmp_name_2)
        if hasattr(self, 'tmp_name_3'):
            os.unlink(self.tmp_name_3)

def t1():
    sr = SurfaceResidues("2pcy.pdb")
    a = sr.surfaceAtomsIndices()
def t2():
    sr = SurfaceResidues("2pcy.pdb")
    a = sr.surfaceAtomsIndices2()

if __name__ == "__main__":
    sr = SurfaceResidues("2pcy.pdb")
    print("\n".join(sr.surfaceResidues()))
    #import timeit
    #print(timeit.timeit("t1()", number=10, setup="from __main__ import t1"))
    #print(timeit.timeit("t2()", number=10, setup="from __main__ import t2"))
